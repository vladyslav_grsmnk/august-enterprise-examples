package embeddable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

//@Entity
//@Table(name = "USERS")
//@Data
//@AllArgsConstructor
//@NoArgsConstructor
//public class User implements Serializable {
//
//    @Id
//    protected Long id;
//
//    protected Address homeAddress;
//
////    @Embedded
////    @AttributeOverrides({
////            @AttributeOverride(name = "street",
////                    column = @Column(name = "BILLING_STREET")),
////            @AttributeOverride(name = "zipcode",
////                    column = @Column(name = "BILLING_ZIPCODE", length = 5)),
////            @AttributeOverride(name = "city",
////                    column = @Column(name = "BILLING_CITY"))
////    })
////    protected Address billingAddress;
//
//
//}
