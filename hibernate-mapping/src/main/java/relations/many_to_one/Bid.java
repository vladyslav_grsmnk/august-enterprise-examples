package relations.many_to_one;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

//@Entity
//@Data
//@Builder
//@NoArgsConstructor
//@AllArgsConstructor
//public class Bid {
//
//    @Id
//    @GeneratedValue
//    private Long id;
//
//    @ManyToOne(fetch = FetchType.LAZY)
//    @JoinColumn(name = "ITEM_ID", nullable = false)
//    private Item item;
//
//    private Double amount;

//}
