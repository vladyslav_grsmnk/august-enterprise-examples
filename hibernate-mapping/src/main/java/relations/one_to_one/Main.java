package relations.one_to_one;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.ArrayDeque;

public class Main {
    public static void main(String[] args) {

        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("mapping");
        EntityManager entityManager = entityManagerFactory.createEntityManager();

        entityManager.getTransaction().begin();

        User user = new User();

        Address address = new Address();
        address.setCity("Kyiv");
        address.setStreet("NV");
        address.setZipcode("07522");

        user.setShippingAddress(address);

        entityManager.persist(user);

        entityManager.getTransaction().commit();
    }
}
