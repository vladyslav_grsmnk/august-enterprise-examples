import advanced.db.dao.impl.VehicleDao;
import advanced.db.entity.Vehicle;
import advanced.exception.DAOException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class VehicleDaoMockTest {
    //table

    private static final String VEHICLE_ID = "VEHICLE_ID";
    private static final String COLOR = "COLOR";
    private static final String WHEELS = "WHEELS";
    private static final String SEATS = "SEATS";

    //scripts

    private static final String INSERT_SQL = "INSERT INTO VEHICLE (COLOR, WHEELS, SEATS) VALUES (?,?,? )";

    private static final String UPDATE_SQL = "UPDATE VEHICLE SET COLOR=?,WHEELS=?,SEATS=? WHERE VEHICLE_ID = ?";
    private static final String SELECT_ALL_SQL = "SELECT * FROM VEHICLE";
    private static final String SELECT_ONE_SQL = "SELECT * FROM VEHICLE WHERE VEHICLE_ID = ?";
    private static final String DELETE_SQL = "DELETE FROM VEHICLE WHERE VEHICLE_ID = ?";

    //expected
    private static final long TEST_ID = 1L;
    private static final Vehicle TEST_VEHICLE_EXPECTED = new Vehicle(TEST_ID, "Red", 4, 4);

    private DataSource mockDataSource = mock(DataSource.class);
    private Connection mockConnection = mock(Connection.class);
    private PreparedStatement mockPreparedStatement = mock(PreparedStatement.class);
    private ResultSet mockResultSet = mock(ResultSet.class);

    private VehicleDao vehicleDao = new VehicleDao(mockDataSource);

    @BeforeEach
    public void before() throws Exception {
        when(mockDataSource.getConnection()).thenReturn(mockConnection);
        when(mockPreparedStatement.executeQuery()).thenReturn(mockResultSet);
        when(mockPreparedStatement.getGeneratedKeys()).thenReturn(mockResultSet);
    }

    @Test
    void whenCreateVehicleShouldReturnVehicle() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);
        when(mockResultSet.next()).thenReturn(true);
        when(mockPreparedStatement.executeUpdate()).thenReturn(1);

        when(mockResultSet.getLong(1)).thenReturn(TEST_ID); //*

        //when
        Vehicle actual = vehicleDao.create(buildTestVehicle());

        //then
        assertEquals(TEST_VEHICLE_EXPECTED, actual);
    }

    @Test
    void whenCreateVehicleShouldThrowExceptionIfIdWasNotGenerated() throws Exception {
        //given
        when(mockConnection.prepareStatement(INSERT_SQL, new String[]{VEHICLE_ID})).thenReturn(mockPreparedStatement);

        //when
        final Executable executable = () -> vehicleDao.create(buildTestVehicle());

        //then
        assertThrows(DAOException.class, executable);
    }

    private Vehicle buildTestVehicle() {
        return Vehicle.builder().color("Red").seats(4).wheels(4).build();
    }
}
