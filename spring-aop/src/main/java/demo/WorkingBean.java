package demo;

import org.springframework.stereotype.Component;

import java.util.concurrent.ThreadLocalRandom;

@Component
public class WorkingBean {

    void doSomeHeavyWork() {

        System.out.println("Doing work");
        try {
            Thread.sleep(ThreadLocalRandom.current().nextInt(100, 10000));

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Finished work");

    }
}
