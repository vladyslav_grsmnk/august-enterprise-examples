package depends_on;

import org.springframework.context.support.GenericXmlApplicationContext;

public class DependsOnDemo {

    public static void main(String... args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext();
        ctx.load("classpath:spring/depends_on/app-context.xml");
        ctx.refresh();

        Singer jimiHendrix = ctx.getBean("jimiHendrix", Singer.class);
        jimiHendrix.sing();

        ctx.close();
    }


}