package message_renderer_setter;

import org.springframework.stereotype.Service;

@Service("provider")
public class HelloWorldMessageProvider implements MessageProvider {

    public HelloWorldMessageProvider() {

        System.out.println(" --> HelloWorldMessageProvider: first.constructor called");
    }

    @Override
    public String getMessage() {
        return "Hello World!";
    }

    public void test() {
        System.out.println("Hello");
    }
}