package naming;

import org.springframework.stereotype.Component;

@Component //bean name - singer
//@Component("jimi")
public class Singer {
    private String lyric = "We found a message in a bottle we were drinking";

    public void sing() {
        System.out.println(lyric);
    }
}
