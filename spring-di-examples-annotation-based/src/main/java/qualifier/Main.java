package qualifier;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(StudentContext.class);
        StudentService t = ctx.getBean(StudentService.class);
        t.printStudentName();
        ctx.close();
    }
}
