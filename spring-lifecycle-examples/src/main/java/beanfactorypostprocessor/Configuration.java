package beanfactorypostprocessor;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("beanfactorypostprocessor")
public class Configuration {
}
