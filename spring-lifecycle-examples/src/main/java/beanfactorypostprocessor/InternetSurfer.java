package beanfactorypostprocessor;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

@Component
public class InternetSurfer {

    @Value("${url}")
    private String url;

    void pingUrl() throws IOException {
        System.out.println(String.format("PINGING %s", this.url));
        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
        connection.setRequestMethod("HEAD");

        int responseCode = connection.getResponseCode();
        System.out.println(responseCode);
    }

}
