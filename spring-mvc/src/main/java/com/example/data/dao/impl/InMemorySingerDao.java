package com.example.data.dao.impl;

import com.example.data.dao.SingerDao;
import com.example.entity.Singer;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

@Repository
public class InMemorySingerDao implements SingerDao {

    private List<Singer> singers;

    @PostConstruct
    void init() {
        Singer singer1 = Singer.builder()
                .firstName("Miles")
                .lastName("Davis")
                .description("American jazz trumpeter, bandleader, and composer")
                .birthDate(new GregorianCalendar(1926, Calendar.MAY, 26).getTime())
                .id(1l)
                .build();

        Singer singer2 = Singer.builder()
                .firstName("Nick")
                .lastName("Cave")
                .description("Australian musician, singer-songwriter, author, screenwriter, " +
                        "composer and occasional actor, best known for fronting the rock band Nick Cave and the Bad Seeds")
                .birthDate(new GregorianCalendar(1957, Calendar.SEPTEMBER, 22).getTime())
                .id(2l)
                .build();

        singers = List.of(singer1, singer2);
    }


    @Override
    public List<Singer> findAll() {
        return this.singers;
    }

    @Override
    public Singer findById(Long id) {
        for (Singer singer : this.singers) {
            if (singer.getId().equals(id)) {
                return singer;
            }
        }
        return null;
    }

    @Override
    public void save(Singer singer) {
//todo
    }

}
