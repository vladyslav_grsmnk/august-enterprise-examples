package com.example.service;

import com.example.entity.Singer;

import java.util.List;

public interface SingerService {

    List<Singer> findAll();

    Singer findById(Long id);

    void save(Singer singer);

}
