package com.example;

import com.example.configuration.ProjectConfiguration;
import com.example.data.dao.SingerDao;
import com.example.data.entity.Singer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.List;


@Slf4j
public class Main {
    public static void main(String[] args) {
        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(ProjectConfiguration.class);
        SingerDao singerDao = ctx.getBean(SingerDao.class);
        listSingers(singerDao.findAll());
        ctx.close();
    }

    private static void listSingers(List<Singer> singers) {
        log.info(" ---- Listing singers:");
        for (Singer singer : singers) {
            log.info(singer.toString());
        }
    }
}
