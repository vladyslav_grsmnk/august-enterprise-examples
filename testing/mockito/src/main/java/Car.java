public class Car {
    private Engine engine;
    private FuelTank fuelTank;

    private boolean isStarted = false;

    public Car(Engine engine, FuelTank fuelTank) {
        this.engine = engine;
        this.fuelTank = fuelTank;
    }

    public void start() {

        if (engine.isRunning()) {
            throw new IllegalStateException("Engine already running");
        }
        if (fuelTank.getFuel() == 0) {
            throw new IllegalStateException("Can't start: no fuel");
        }

        engine.start();

        if (!engine.isRunning()) {
            throw new IllegalStateException("Started engine but isn't running");
        }

        isStarted = true;

    }

    public boolean isCarStarted(){
        return this.isStarted;
    }

    public boolean isEngineRunning() {
        return engine.isRunning();
    }
}